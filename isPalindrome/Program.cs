﻿/*
 * C# Program to Reverse a String without using Reverse function
 */
using System;
class Program
{
    static void Main(string[] args)
    {
        string Str, reversestring = "";
        int Length;
        Console.Write("Enter number : ");
        Str = Console.ReadLine();
        Length = Str.Length - 1;
        while (Length >= 0)
        {
            reversestring = reversestring + Str[Length];
            Length--;
        }
        if (reversestring == Str)
        {
            Console.WriteLine("isPalindrome");
        }
        else
        {
            Console.WriteLine("notPalindrome");
        };
        Console.ReadKey();
    }
}